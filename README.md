# What's here
- [uniqueLastThree.py](./uniqueLastThree.py) è l'evoluzione che generalizza [script_21.py](./old/script_21.py) per iterare sui tre più recenti
- [uniqueFirstTwo.py](./uniqueFirstTwo.py) è il partner naturale di [uniqueLastThree.py](./uniqueLastThree.py) per gli altri due anni
- [meccToSchool.py](./meccToSchool.py) pur non essendo direttamente nato per rispondere alle esigenze primarie, si trova bene in questo repository. Il suo scopo è convertire un codice meccanografico nel nome di un istituto


## Old stuffs
- [script_21.py](./old/script_21.py) è uno script che permette di tirare giù i candidati idonei da un certo sito. Questo è il primo che ho fatto. Visto che ci sono piccole modifiche ma sono solo 5 script, li faccio a mano. **Tutti gli altri sono derivati da questo**.
- [callAll.sh](./old/callAll.sh) è un modo becero per chiamarli

## Known bugs
- Se ci sono nomi con lo spazio, viene trimmato
- Nella data, tutti gli spazi sono trimmati

Potri risolverli? Si, ma non voglio
