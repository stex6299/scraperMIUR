#!/usr/bin/env python3
import sys, os, re, glob

import httplib2
from bs4 import BeautifulSoup



###### START SNIPPET ######
# ref: https://stackoverflow.com/questions/71603314/ssl-error-unsafe-legacy-renegotiation-disabled
import requests
import urllib3
import ssl


class CustomHttpAdapter (requests.adapters.HTTPAdapter):
    # "Transport adapter" that allows us to use custom ssl_context.

    def __init__(self, ssl_context=None, **kwargs):
        self.ssl_context = ssl_context
        super().__init__(**kwargs)

    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = urllib3.poolmanager.PoolManager(
            num_pools=connections, maxsize=maxsize,
            block=block, ssl_context=self.ssl_context)


def get_legacy_session():
    ctx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
    ctx.options |= 0x4  # OP_LEGACY_SERVER_CONNECT
    session = requests.session()
    session.mount('https://', CustomHttpAdapter(ctx))
    return session

###### END SNIPPET ######


# A titolo esemplificativo
lstCodici = ["VIPS05000N",]# "BOPC02000A", "MCPS02000N"]
outputDict = {}


# Url tipo: https://cercalatuascuola.istruzione.it/cercalatuascuola/ricerca/risultati?rapida=BOPC02000A&tipoRicerca=RAPIDA&gidf=1


for mecc in lstCodici:
    
    print(mecc)
    
    currStartUrl = f"https://cercalatuascuola.istruzione.it/cercalatuascuola/ricerca/risultati?rapida={mecc}&tipoRicerca=RAPIDA&gidf=1"
    print(currStartUrl)
    
    # Ottengo la pagina con il form da cui voglio prendere tutti i campi dei form
    # http = httplib2.Http() # Senno crashava alla seconda richiesta
    # status, response = http.request(currStartUrl) 

    # Faccio la richiesta HTTP al sito
    response = get_legacy_session().get(currStartUrl)

    # Costruisco la risposta HTML
    myResp = ""
    for a in response:
        # print(a.decode())
        try:
            myResp += a.decode()
        except:
            print("vvvvvvvvvvvvvvvvvvvvvv")
            print(a)
            print("^^^^^^^^^^^^^^^^^^^^^^")
    # print(list(response))
    



    # Creo il parser
    soup = BeautifulSoup(myResp, "html.parser")


    lstTD = soup.find_all("td", attrs={"data-col-1": "Istituto Principale"})[0]
    # print(lstSettoriRaw)
    
    for el in lstTD.findChildren("a"):
        # print(el.contents)
        myName = [e.strip() for e in el.contents if e.strip() != ""][0]
    print(f"Nome istituto trovato: {myName} per il codice {mecc}\n\n")
    
    
    # Popolo il tuo dizionario
    outputDict[mecc] = myName


