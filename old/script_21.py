#!/usr/bin/env python3
import sys, os, re, glob

import httplib2
from bs4 import BeautifulSoup


# ***** Ottengo la lista dei settori concorsuali *****

# Ottengo la pagina con il form da cui voglio prendere i settori concorsuali
http = httplib2.Http() # Senno crashava alla seconda richiesta
status, response = http.request("https://abilitazione.mur.gov.it/public/pubblicarisultati_2021.php") # @@@

# Creo il parser
soup = BeautifulSoup(response, "html.parser")

# Ottengo tutti i tag 
# Specificando gli attributi sono sicuro ne trovi solo una
lstSettoriRaw = soup.find_all("select", attrs={"name": "settore"})[0]
# print(lstSettoriRaw)


# Lista dei settori concorsuali
lstSettori = [el.get("value") for el in lstSettoriRaw.findChildren("option") if el.get("value").strip() != ""]

print("\n".join(lstSettori))


# ***** SCRAPERO LA LISTA DI PERSONE *****

# Esempio link
"""
https://asn21.cineca.it/pubblico/miur/esito-abilitato/03%252FC1/2/5

* Base: https://asn21.cineca.it/pubblico/miur/esito-abilitato/
* Due lettere settore
* / MAPPED IN %252F
* /
* Fascia (1 or 2)
* /
* Quadrimestre (1, 2, 3, 4, 5)
"""

# @@@ Change for each year
baseUrl = r"https://asn21.cineca.it/pubblico/miur/esito-abilitato"

lstFascia = [1, 2]
lstQuadrimestre = [1, 2, 3, 4, 5]

slash = "/"
replaceSlash = r"%252F"

# @@@ Change for each year
outfile = "./year_21.out"
if os.path.exists(outfile):
    print("Rimuovo l'outfile che esiste già...")
    os.remove(outfile)


# Big loop
for sett in lstSettori:
    for fasc in lstFascia:
        for quad in lstQuadrimestre: 
            
            myUrl = f"{baseUrl}/{sett.replace(slash, replaceSlash)}/{fasc}/{quad}"
            print (f"Sto per processare --- {myUrl}")
            
            # Ottengo la pagina
            http = httplib2.Http() # Senno crashava alla seconda richiesta
            status, response = http.request(myUrl)
            
            soup = BeautifulSoup(response, "html.parser")                


            # Output file
            with open(outfile, "a") as outf:
                
                # Inizio il file scrivendo l'url
                outf.write(myUrl)
                outf.write("\n")
                
                # Header fatto meglio
                outf.write(f"Settore concorsuale: {sett}\tFascia: {fasc}\tQuadrimestre: {quad}\n")
                
                try:
                    # Le persone sono in un oggetto tbody, l'unico per la precisione
                    tabellaPersone = soup.find_all("tbody")[0]

                    
                    # Ogni tr è una riga
                    for persona in tabellaPersone.findChildren("tr"):
                        
                        # Ogni campo (nome, cognome, si, data) sono un td
                        for eel in persona.findChildren("td"):
                            # print(eel.text.strip())
                            entryToSave = re.sub(r"\s+", "", eel.text.strip()) 
                            entryToSave += "\t" 
                            
                            print(entryToSave)
                            
                            # Salvo la entry nel file
                            outf.write(entryToSave)
                            
                        # Riga vuota tra ogni persona    
                        outf.write("\n")
                except IndexError as ex:
                    print("No people in here!\n Never mind, skip this page")           
                    continue             
                        
                # Alla fine del record lascio una riga vuota    
                outf.write("\n")
            