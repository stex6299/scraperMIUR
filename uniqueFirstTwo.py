#!/usr/bin/env python3
import sys, os, re, glob

import httplib2
from bs4 import BeautifulSoup

dictYear = {
    12 : "https://abilitazione.mur.gov.it/public/pubblicarisultati.php",
    13 : "https://abilitazione.mur.gov.it/public/pubblicarisultati_2013.php",
}
#    12 : "https://abilitazione.cineca.it/ministero.php/public/esitoAbilitati/settore/02%252FC1/fascia/2",
dictEsiti = {
    12 : "https://abilitazione.cineca.it/ministero.php/public/esitoAbilitati",
    13 : "https://asn.cineca.it/ministero.php/public/esitoAbilitati",
}


# Iterate over each year
for key in dictYear.keys():
    
    # Pagina col form per la query
    currStartUrl = dictYear[key]
    print(currStartUrl)

    # Ottengo la pagina con il form da cui voglio prendere tutti i campi dei form
    http = httplib2.Http() # Senno crashava alla seconda richiesta
    status, response = http.request(currStartUrl) 

    # Creo il parser
    soup = BeautifulSoup(response, "html.parser")

    # Ottengo tutti i tag 
    # Specificando gli attributi sono sicuro ne trovi solo una
    
    # **** Estraggo le possibili scelte del form ****
    
    # Lista dei settori concorsuali
    lstSettoriRaw = soup.find_all("select", attrs={"name": "settore"})[0]
    lstSettori = [el.get("value") for el in lstSettoriRaw.findChildren("option") if el.get("value").strip() != ""]

    print("\n".join(lstSettori))
    
    # Fasce 
    lstFasceRaw = soup.find_all("select", attrs={"name": "fascia"})[0]
    lstFasce = [el.get("value") for el in lstFasceRaw.findChildren("option") if el.get("value").strip() != ""]
    print("\n".join(lstFasce))
    
    # # Sessioni, o quadrimestri
    # lstSessioniRaw = soup.find_all("select", attrs={"name": "sessione"})[0]
    # lstSessioni = [el.get("value") for el in lstSessioniRaw.findChildren("option") if el.get("value").strip() != ""]
    # print("\n".join(lstSessioni))
    
    # ***** Estraggo il risultato per ciascuna combinazione *****
    
    # Pagina con i risultati
    # baseUrl = f"https://asn{key}.cineca.it/pubblico/miur/esito-abilitato"
    
    
    # Alcune cose per costruire il percorso corretto
    # In particolare, questa è la ricetta per il settore concorsuale
    slash = "/"
    replaceSlash = r"%252F"

    # Outfile
    outfile = f"./year_{key}.out"
    if os.path.exists(outfile):
        print("Rimuovo l'outfile che esiste già...")
        os.remove(outfile)
        
        
        
        
            
    # Big loop
    for sett in lstSettori:
        for fasc in lstFasce:
            #for quad in lstSessioni: 
                
                # myUrl = f"{baseUrl}/{sett.replace(slash, replaceSlash)}/{fasc}/{quad}"
                myUrl = f"{dictEsiti[key]}/settore/{sett.replace(slash, replaceSlash)}/fascia/{fasc}"
                print (f"Sto per processare --- {myUrl}")
                
                # Ottengo la pagina
                http = httplib2.Http() # Senno crashava alla seconda richiesta
                status, response = http.request(myUrl)
                
                soup = BeautifulSoup(response, "html.parser")                


                # Output file
                with open(outfile, "a") as outf:
                    
                    # Inizio il file scrivendo l'url
                    outf.write(myUrl)
                    outf.write("\n")
                    
                    # Header fatto meglio
                    outf.write(f"Settore concorsuale: {sett}\tFascia: {fasc}\n")
                    
                    try:
                        # Le persone sono in un oggetto tbody, l'unico per la precisione
                        tabellaPersone = soup.find_all("tbody")[0]

                        
                        # Ogni tr è una riga
                        for persona in tabellaPersone.findChildren("tr"):
                            
                            # Ogni campo (nome, cognome, si, data) sono un td
                            for eel in persona.findChildren("td"):
                                # print(eel.text.strip())
                                entryToSave = re.sub(r"\s+", "", eel.text.strip()) 
                                entryToSave += "\t" 
                                
                                print(entryToSave)
                                
                                # Salvo la entry nel file
                                outf.write(entryToSave)
                                
                            # Riga vuota tra ogni persona    
                            outf.write("\n")
                    except IndexError as ex:
                        print("No people in here!\n Never mind, skip this page")           
                        continue             
                            
                    # Alla fine del record lascio una riga vuota    
                    outf.write("\n")
                

